FROM node:12.16.1-alpine

RUN apk add --update \
    curl \
    && rm -rf /var/cache/apk/*

RUN apk add --no-cache \
    python3 \
    py3-pip \
    && pip3 install --upgrade pip \
    && pip3 install \
    awscli \
    && rm -rf /var/cache/apk/*

RUN aws --version   # Just to make sure its installed alright

RUN curl -o kubectl https://amazon-eks.s3.us-west-2.amazonaws.com/1.18.9/2020-11-02/bin/linux/amd64/kubectl

RUN chmod u+x ./kubectl && mv kubectl /bin/kubectl

RUN echo 'export PATH=$PATH:$HOME/bin' >> ~/.bash_profile

# to run the image: 

# 1. build the image if its not already built
# docker build -f aws-kubectl.dockerfile -t awscli-kubectl .


# 2. go to PowerShell 
# add a `.env` file with:

###############################
#AWS_ACCESS_KEY_ID=YOUR_ID
#AWS_SECRET_ACCESS_KEY=YOUR_KEY
#AWS_DEFAULT_REGION=YOUR_DEFAULT_REGION
#CLUSTER_NAME=YOUR_CLUSTER_NAME_APP
################################

# replace YOUR_ID, YOUR_KEY, AWS_DEFAULT_REGION and CLUSTER_NAME with the actual values


# 3. run the following command in PowerShell 
# docker run --env-file .env -it -u 0 --rm awscli-kubectl  /bin/ash -c 'aws eks update-kubeconfig --region $AWS_DEFAULT_REGION --name $CLUSTER_NAME && /bin/ash'

# 4. verify your image is working
# kubectl get pods --all-name-spaces